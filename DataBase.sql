
/****** Object:  Database [WebShop]    Script Date: 23/06/2017 4:58:30 p. m. ******/
CREATE DATABASE [WebShop]


USE [WebShop]
GO

/****** Object:  Table [dbo].[Product]    Script Date: 23/06/2017 5:00:05 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product](
	[IdProduct] [int] IDENTITY(1,1) NOT NULL,
	[Number] [nvarchar](150) NOT NULL,
	[Title] [nvarchar](150) NOT NULL,
	[Price] [float] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[IdProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
