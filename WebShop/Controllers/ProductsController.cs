﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebShop.Models;

namespace WebShop.Controllers
{
	public class ProductsController : Controller
	{
		private WebShopEntities db = new WebShopEntities();

		// GET: Products
		public async Task<ActionResult> Index(int typeStorage = 0)
		{
			Session["typeStorage"] = typeStorage;
			ViewBag.typeStorage = typeStorage;
			if (typeStorage == 0)
			{
				return View(await db.Product.ToListAsync());
			}
			else
			{
				List<Product> productList = new List<Product>();
				if (Session["ProductList"] == null)
				{
					productList = null;
				}
				else
				{
					productList = (List<Product>)Session["ProductList"];
					
				}

				return View(productList);
			}

		}


		// GET: Products/Create
		public ActionResult Create()
		{
			ViewBag.typeStorage = Session["typeStorage"];
			return View();
		}

		// POST: Products/Create    
		[HttpPost]
		public async Task<ActionResult> Create([Bind(Include = "IdProduct,Number,Title,Price")] Product product)
		{
			int id = 0;
			if (Session["typeStorage"] != null)
			{
				id = (int)Session["typeStorage"];
			}
			
			if (Session["ProductList"] == null && id == 0)
			{
				
					if (product.Number == null)
					{
						ModelState.AddModelError("Number", "Required");
					}

				if (product.Title == null)
				{
					ModelState.AddModelError("Title", "Required");
				}

			

				if (ModelState.IsValid)
				{
					db.Product.Add(product);
					await db.SaveChangesAsync();
					return RedirectToAction("Index");
				}
				else
				{
					return View(product);
				}
			}
			else
			{
				List<Product> productList = new List<Product>();
				if (Session["ProductList"] != null)
				{
					productList = (List<Product>)Session["ProductList"];
				}
				
				productList.Add(product);
				Session["ProductList"] = productList;
				return RedirectToAction("Index",new { typeStorage = id } );
			}
			
		}


		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
